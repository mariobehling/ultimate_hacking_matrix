/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "usb_device.h"
#include "bsp.h"

/* USER CODE BEGIN Includes */
typedef union
{
  uint16_t half_word;
  uint8_t byte[2];
} union_half_word_byte_t;

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

uint32_t g_tim7_state;
uint8_t g_switch_config = 0; // the switch is 8 bit, one bit for usb upgrade. available from 0-127
uint32_t g_switch_config_previous = 0;
uint32_t g_scan_row = 0; // 0 to KEYBOARD_ROW_NUMBER - 1
uint32_t g_scan_row_sampling = 0; // 0 or other
uint32_t g_key_buf[ (KEYBOARD_ROW_NUMBER) + 1] = {0}; // all of contents are 0s, there is 1 row of switches
uint32_t g_key_buf_transposed[KEYBOARD_COLUMN_NUMBER] = {0};

uint32_t g_fn_mask[KEYBOARD_ROW_NUMBER] = {0};

uint16_t g_key_logger[NUMBER_OF_KEYS] = {0};
// all of them are initialized with 0s, this is compiler spefic

// g_fn_mask[K_FN_LEFT_ROW] = g_fn_mask[K_FN_LEFT_ROW] | (1 << K_FN_LEFT_COLUMN);
uint8_t g_usb_report_buf[HID_BUFFER_SIZE];
uint8_t g_usb_report_buf_previous[HID_BUFFER_SIZE];

uint8_t g_uart1_rx_buf[UART1_RX_BUF_SIZE] = {0};
uint8_t g_uart1_tx_buf[UART1_TX_BUF_SIZE] = {0};
uint32_t g_running_time_ms = 0;
uint8_t g_fn_list[KEYBOARD_MAX_FN_KEY_NUMBER][2] = {0}; // [0] -> row, [1] -> column

uint8_t g_led_scan_row = 0;
uint16_t g_led_scan_row_pattern[KEYBOARD_ROW_NUMBER] = {0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff};


// Notice: fn key should remain fn, when fn is pressed
uint16_t g_key_lut[KEYBOARD_CONFIGURATION_COUNT][KEYBOARD_LAYER_NUMBER][KEYBOARD_ROW_NUMBER][KEYBOARD_COLUMN_NUMBER] = \
{
  // config 0 ******************** QWERT 0  ********************
  {
    {
      // Config 2 QWERT normal *************************************************
      {K_UNDEFINED,     K_UNDEFINED,     K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_1,             K_2,             K_3,             K_4,             K_5,             K_6,             K_7,             K_8,             K_9,             K_0,             K_MINUS,         K_EQUAL,         K_INSERT,        K_DEL},
      {K_TAB,           K_TAB,           K_Q,             K_W,             K_E,             K_R,             K_T,             K_Y,             K_U,             K_I,             K_O,             K_P,             K_BRACKET_LEFT,  K_BRACKET_RIGHT, K_BACKSPACE,     K_BACK_SLASH},
      {K_CTRL_LEFT,     K_CTRL_LEFT,     K_A,             K_S,             K_D,             K_F,             K_G,             K_H,             K_J,             K_K,             K_L,             K_SEMICOLON,     K_QUOTE,         K_ENTER,         K_HOME,          K_END},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_Z,             K_X,             K_C,             K_V,             K_B,             K_N,             K_M,             K_COMMA,         K_PERIOD,        K_SLASH,         K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_CTRL_LEFT,     K_FN_LEFT,       K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    },

    {
      // Config 2 QWERT FN *****************************************************
      {K_UNDEFINED,     K_UNDEFINED,     K_PLAY_PREVIOUS, K_PLAY_PLAY,     K_PLAY_NEXT,     K_PLAY_STOP,     K_F5,            K_F6,            K_F7,            K_NUM_LOCK,      K_SCROLL_LOCK,   K_VOL_DOWN,      K_VOL_UP,        K_MUTE,          K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_INSERT,        K_DEL},
      {K_PRINTSCREEN,   K_CAPS_LOCK,     K_Q,             K_W,             K_END,           K_R,             K_T,             K_Y,             K_BACKSPACE,     K_INSERT,        K_UP,            K_UP,            K_BRACKET_LEFT,  K_BRACKET_RIGHT, K_BACKSPACE,     K_BACK_SLASH},
      {K_PAUSE_BREAK,   K_CTRL_LEFT,     K_HOME,          K_S,             K_DEL,           K_RIGHT,         K_G,             K_H,             K_HOME,          K_PAGE_UP,       K_LEFT,          K_RIGHT,         K_QUOTE,         K_ENTER,         K_HOME,          K_END},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_Z,             K_X,             K_C,             K_V,             K_LEFT,          K_DOWN,          K_END,           K_PAGE_DOWN,     K_DOWN,          K_SLASH,         K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_CTRL_LEFT,     K_FN_LEFT,       K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    }
  },
  // Config 1 QWERT ************************************************************
  {
    {
      // Config 1 QWERT normal *************************************************
      {K_UNDEFINED,     K_UNDEFINED,     K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_1,             K_2,             K_3,             K_4,             K_5,             K_6,             K_7,             K_8,             K_9,             K_0,             K_MINUS,         K_EQUAL,         K_INSERT,        K_DEL},
      {K_TAB,           K_TAB,           K_Q,             K_W,             K_E,             K_R,             K_T,             K_Y,             K_U,             K_I,             K_O,             K_P,             K_BRACKET_LEFT,  K_BRACKET_RIGHT, K_BACK_SLASH,    K_BACKSPACE},
      {K_CTRL_LEFT,     K_CTRL_LEFT,     K_A,             K_S,             K_D,             K_F,             K_G,             K_H,             K_J,             K_K,             K_L,             K_SEMICOLON,     K_QUOTE,         K_ENTER,         K_HOME,          K_END},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_Z,             K_X,             K_C,             K_V,             K_B,             K_N,             K_M,             K_COMMA,         K_PERIOD,        K_SLASH,         K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_FN_LEFT,       K_CTRL_LEFT,     K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    },

    {
      // Config 1 QWERT FN *****************************************************
      {K_UNDEFINED,     K_UNDEFINED,     K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_NUM_LOCK,      K_SCROLL_LOCK,   K_VOL_DOWN,      K_VOL_UP,        K_MUTE,          K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_INSERT,        K_DEL},
      {K_PRINTSCREEN,   K_CAPS_LOCK,     K_Q,             K_W,             K_END,           K_R,             K_T,             K_Y,             K_BACKSPACE,     K_I,             K_UP,            K_UP,            K_BRACKET_LEFT,  K_BRACKET_RIGHT, K_BACK_SLASH,    K_BACKSPACE},
      {K_PAUSE_BREAK,   K_CTRL_LEFT,     K_HOME,          K_S,             K_DEL,           K_RIGHT,         K_G,             K_H,             K_HOME,          K_PAGE_UP,       K_LEFT,          K_RIGHT,         K_QUOTE,         K_ENTER,         K_HOME,          K_END},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_Z,             K_X,             K_C,             K_V,             K_LEFT,          K_DOWN,          K_END,           K_PAGE_DOWN,     K_DOWN,          K_SLASH,         K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_FN_LEFT,       K_CTRL_LEFT,     K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    }
  },
  // Config 2 undefined ************************************************************
  {
    {
      // BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT, K_BRACKET_RIGHT
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED}
    },
    {
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED},
      {K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED,     K_UNDEFINED}
    }
  },
  // config 3 ******************** Dvorak ********************
  {
    {
      // ****************************** normal ******************************
      {K_UNDEFINED,     K_UNDEFINED,     K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_1,             K_2,             K_3,             K_4,             K_5,             K_6,             K_7,             K_8,             K_9,             K_0,             K_BRACKET_LEFT,  K_BRACKET_RIGHT, K_INSERT,        K_DEL},
      {K_TAB,           K_TAB,           K_QUOTE,         K_COMMA,         K_PERIOD,        K_P,             K_Y,             K_F,             K_G,             K_C,             K_R,             K_L,             K_SLASH,         K_EQUAL,         K_BACKSPACE,     K_BACK_SLASH},
      {K_CTRL_LEFT,     K_CTRL_LEFT,     K_A,             K_O,             K_E,             K_U,             K_I,             K_D,             K_H,             K_T,             K_N,             K_S,             K_MINUS,         K_ENTER,         K_ENTER,         K_ENTER},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_SEMICOLON,     K_Q,             K_J,             K_K,             K_X,             K_B,             K_M,             K_W,             K_V,             K_Z,             K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_FN_LEFT,       K_CTRL_LEFT,     K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    },

    {
      // ******************************   FN   ******************************
      {K_UNDEFINED,     K_UNDEFINED,     K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_UNDEFINED,     K_UNDEFINED},
      {K_ESC,           K_BACK_QUOTE,    K_F1,            K_F2,            K_F3,            K_F4,            K_F5,            K_F6,            K_F7,            K_F8,            K_F9,            K_F10,           K_F11,           K_F12,           K_INSERT,        K_DEL},
      {K_PRINTSCREEN,   K_CAPS_LOCK,     K_Q,             K_COMMA,         K_PERIOD,        K_UP,            K_Y,             K_RIGHT,         K_G,             K_C,             K_R,             K_L,             K_SLASH,         K_EQUAL,         K_BACKSPACE,     K_BACK_SLASH},
      {K_CTRL_LEFT,     K_CTRL_LEFT,     K_HOME,          K_O,             K_END,           K_BACKSPACE,     K_I,             K_DEL,           K_H,             K_T,             K_DOWN,          K_S,             K_MINUS,         K_ENTER,         K_ENTER,         K_ENTER},
      {K_SHIFT_LEFT,    K_SHIFT_LEFT,    K_SEMICOLON,     K_Q,             K_J,             K_K,             K_X,             K_LEFT,          K_M,             K_W,             K_V,             K_Z,             K_FN_RIGHT,      K_PAGE_UP,       K_UP,            K_PAGE_DOWN},
      {K_FN_LEFT,       K_CTRL_LEFT,     K_SUPER_LEFT,    K_ALT_LEFT,      K_UNDEFINED,     K_SPACE,         K_SPACE,         K_SPACE,         K_SPACE,         K_APPLICATION,   K_ALT_RIGHT,     K_CTRL_RIGHT,    K_SHIFT_RIGHT,   K_LEFT,          K_DOWN,          K_RIGHT}
    }
  }
};


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config (void);
static void MX_GPIO_Init (void);
static void MX_I2C1_Init (void);
static void MX_SPI1_Init (void);
static void MX_SPI2_Init (void);
static void MX_SPI3_Init (void);
static void MX_TIM3_Init (void);
static void MX_TIM4_Init (void);
static void MX_USART1_UART_Init (void);
static void MX_TIM7_Init (void);
static void MX_TIM6_Init (void);

void HAL_TIM_MspPostInit (TIM_HandleTypeDef *htim);


/* USER CODE BEGIN PFP */
void transpose_matrix (uint32_t *input_matrix, uint32_t *output_matrix, uint32_t column_number_of_input_matrix, uint32_t row_number_of_input_matrix);
void reverse_matrix_with_mask (uint32_t matrix[], uint32_t column_number);
int32_t keyboard_is_fn_pressed (void);
int32_t keyboard_pressed_key_count (void);
void keyboard_generate_report (void);
void fn_mask_init (void);
void TIM7_IRQHandler_callback (void);
void TIM6_DAC_IRQHandler_callback (void);
void keyboard_drive_column (uint32_t column);
void led_drive_row (uint16_t pattern, uint16_t row_number);
// Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc/usbd_hid.h line 129
extern uint8_t USBD_HID_SendReport (USBD_HandleTypeDef *pdev, uint8_t *report, uint16_t len);


/* Private function prototypes -----------------------------------------------*/

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/**
 * @brief  transpose matrix, take care of array overflow
 * @param  None
 * @retval None
 * @intput &matrix[16]
 * @output &matrix[7]
 */
void transpose_matrix (uint32_t input_matrix[], uint32_t output_matrix[], uint32_t column_number_of_input_matrix, uint32_t row_number_of_input_matrix)
{
  // TODO: limit the row and column number before doing matrix
  // transform of switch is not included in KEYBOARD_ROW_NUMBER, there should be one row more


  // basicly 7 * 16
  uint32_t column_number_of_output_matrix  = row_number_of_input_matrix ; // 16
  uint32_t row_number_of_output_matrix  =  column_number_of_input_matrix; // 7
  if ((column_number_of_output_matrix > KEYBOARD_COLUMN_NUMBER)
      || (row_number_of_output_matrix > (KEYBOARD_ROW_NUMBER + 1)))
  {
    _Error_Handler (__FILE__, __LINE__);
    return;
  }
  else
  {
    for (int i = 0; i < row_number_of_output_matrix; i++)
    {
      uint32_t mask = 1 << i;
      output_matrix[i] = \
                         (((input_matrix[0] & mask) >> i) << 0)
                         | (((input_matrix[1] & mask) >> i) << 1)
                         | (((input_matrix[2] & mask) >> i) << 2)
                         | (((input_matrix[3] & mask) >> i) << 3)
                         | (((input_matrix[4] & mask) >> i) << 4)
                         | (((input_matrix[5] & mask) >> i) << 5)
                         | (((input_matrix[6] & mask) >> i) << 6)
                         | (((input_matrix[7] & mask) >> i) << 7)
                         | (((input_matrix[8] & mask) >> i) << 8)
                         | (((input_matrix[9] & mask) >> i) << 9)
                         | (((input_matrix[10] & mask) >> i) << 10)
                         | (((input_matrix[11] & mask) >> i) << 11)
                         | (((input_matrix[12] & mask) >> i) << 12)
                         | (((input_matrix[13] & mask) >> i) << 13)
                         | (((input_matrix[14] & mask) >> i) << 14)
                         | (((input_matrix[15] & mask) >> i) << 15);
    }
  }
}

/**
 * @brief  reverse matrix
 * @param  None
 * @retval fn_pressed
 * @intput g_key_buf[KEYBOARD_ROW_NUMBER]
 * @output
 */
void reverse_matrix_with_mask (uint32_t matrix[], uint32_t column_number)
{
  uint32_t mask = (1 << KEYBOARD_COLUMN_NUMBER) - 1;
  for (int i = 0; i < column_number; i++)
  {
    matrix[i] = (~ (matrix[i]))&mask;
  }
}

/**
 * @brief  Check if FN is pressed
 * @param  None
 * @retval fn_pressed
 * @intput g_key_buf[KEYBOARD_ROW_NUMBER]
 * @output
 */
int32_t keyboard_is_fn_pressed (void)
{
  for (int i = 0; i < KEYBOARD_ROW_NUMBER; i++)
  {
    if (0 != (g_key_buf[i] & g_fn_mask[i]))
    {
      return 1; // pressed
    }
  }
  return 0;  //not pressed
}


/**
 * @brief  Check how many keys are pressed
 * @param  None
 * @retval number_keys
 * @intput g_key_buf[KEYBOARD_ROW_NUMBER]
 * @output
 */
int32_t keyboard_pressed_key_count (void)
{
  int32_t cnt;
  // very fast and cool method to count 1s in variable
  // https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
  uint32_t v; // count bits set in this (32-bit value)
  uint32_t c; // store the total here
  const uint32_t S[] = {1, 2, 4, 8, 16}; // Magic Binary Numbers
  const uint32_t B[] = {0x55555555, 0x33333333, 0x0F0F0F0F, 0x00FF00FF, 0x0000FFFF};

  cnt = 0;
  for (int i = 0; i < KEYBOARD_ROW_NUMBER; i++)
  {
    v = g_key_buf[i];
    c = v - ((v >> 1) & B[0]);
    c = ((c >> S[1]) & B[1]) + (c & B[1]);
    c = ((c >> S[2]) + c) & B[2];
    c = ((c >> S[3]) + c) & B[3];
    c = ((c >> S[4]) + c) & B[4];
    cnt += c;
  }
  return cnt;
}


/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 * @intput g_key_buf[KEYBOARD_ROW_NUMBER], g_usb_report_buf[HID_BUFFER_SIZE];
 * @output g_key_buf[KEYBOARD_ROW_NUMBER], g_usb_report_buf[HID_BUFFER_SIZE];
 */
void keyboard_generate_report (void)
{
  // const uint32_t key_buf[KEYBOARD_ROW_NUMBER];// = g_key_buf;
  // uint32_t p = key_buf;
  // memcpy(p, g_key_buf, sizeof(g_key_buf) * sizeof(g_key_buf[0]));

  int layer = 0;
  // clear USB report buffer
  for (int i = 0; i < HID_BUFFER_SIZE; i++)
  {
    g_usb_report_buf_previous[i] = g_usb_report_buf[i];
    g_usb_report_buf[i] = 0;
  }

  // more than 6 keys pressed?
  int cnt = keyboard_pressed_key_count();
  if (cnt > 6)
  {
    return;
  }
  // key pressed <= 6
  else
  {
    // check if FN pressed, side effect, read global variable
    if (0 != keyboard_is_fn_pressed())
    {
      layer = 1;
    }
    // else
    // {
    //   layer = 0;
    // }

    // modifier key pressed
    // Ctrl or Alt or Shift pressed
    // side effect, read global variable
    // all normal keys
    for (int i = 0; i < KEYBOARD_ROW_NUMBER; i++)
    {
      for (int j = 0; j < KEYBOARD_COLUMN_NUMBER; j++)
      {
        if (0 != (g_key_buf[i]  & (1 << j))) // key pressed
        {
          uint16_t report = g_key_lut[g_switch_config][layer][i][j];

          // fn key is pressed
          if (0 != (report & 0x200)) // all fn keys are defined by 0x200 + index
          {
            ;
          }
          // modifier key pressed
          else if (0 != (report & 0x100)) // all modifiers are defined with 0x100 + Usage ID
          {
            // please note time consuming here
            switch (report)
            {
              case K_CTRL_LEFT:
                g_usb_report_buf[0] |= 1 << 0;
                break; // ctrl_left
              case K_SHIFT_LEFT:
                g_usb_report_buf[0] |= 1 << 1;
                break; // shift_left
              case K_ALT_LEFT:
                g_usb_report_buf[0] |= 1 << 2;
                break; // alt_left
              case K_SUPER_LEFT:
                g_usb_report_buf[0] |= 1 << 3;
                break; // super_left
              case K_CTRL_RIGHT:
                g_usb_report_buf[0] |= 1 << 4;
                break; // ctrl_right
              case K_SHIFT_RIGHT:
                g_usb_report_buf[0] |= 1 << 5;
                break; // shift_right
              case K_ALT_RIGHT:
                g_usb_report_buf[0] |= 1 << 6;
                break; // alt_right
              case K_SUPER_RIGHT:
                g_usb_report_buf[0] |= 1 << 7;
                break; // super_right
              default:
                break;
            }
          }
          else
          {
            for (int k = 2; k < HID_BUFFER_SIZE; k++)
            {
              if (0 == g_usb_report_buf[k]) // no data in this byte
              {
                g_usb_report_buf[k] = g_key_lut[g_switch_config][layer][i][j] % 256;
                break;
              }
            }
          }
        }
      }
    }
  }

  //****************************************************************************
  for (int i = 2; i < HID_BUFFER_SIZE; i++)
  {
    uint16_t key = g_usb_report_buf[i];
    int found = 0;

    // try to find difference between the previous buffer and the current buffer
    for (int j = 2; j < HID_BUFFER_SIZE; j++)
    {
      // found = 0;
      if (key == g_usb_report_buf_previous[j])
      {
        found = 1;
        break;
      }
    }

    // not found, key rise detected
    if (0 == found)
    {
      // key_logger_add (key);
    }
  }

  //****************************************************************************
  return;
}


/**
 * @brief  Read pressed keys in one column. Side effect (IO)
 * @param  uint32_t column_pressed_keys;
 * @retval None
 * @input  HAL_GPIO_ReadPin (KEY_ROW_xx_GPIO_Port, KEY_ROW_xx_Pin)
 * @output None
 */
__IO uint32_t keyboard_read_column()
{
  __IO uint32_t column_pressed_keys = \
                                      HAL_GPIO_ReadPin (KEY_ROW_00_GPIO_Port, KEY_ROW_00_Pin) \
                                      | (HAL_GPIO_ReadPin (KEY_ROW_01_GPIO_Port, KEY_ROW_01_Pin) << 1)\
                                      | (HAL_GPIO_ReadPin (KEY_ROW_02_GPIO_Port, KEY_ROW_02_Pin) << 2)\
                                      | (HAL_GPIO_ReadPin (KEY_ROW_03_GPIO_Port, KEY_ROW_03_Pin) << 3)\
                                      | (HAL_GPIO_ReadPin (KEY_ROW_04_GPIO_Port, KEY_ROW_04_Pin) << 4)\
                                      | (HAL_GPIO_ReadPin (KEY_ROW_05_GPIO_Port, KEY_ROW_05_Pin) << 5)\
                                      | (HAL_GPIO_ReadPin (KEY_ROW_06_GPIO_Port, KEY_ROW_06_Pin) << 6);
  return column_pressed_keys;
}


/**
 * @brief  Generate a mask for the current fn layout, has side effect
 * @param  None
 * @retval none
 * @intput g_key_buf
 * @output g_fn_mask
 */
void fn_mask_init (void)
{
  for (int i = 0; i < KEYBOARD_ROW_NUMBER; i++)
  {
    g_fn_mask[i] = 0;
  }

  for (int i = 0; i < KEYBOARD_ROW_NUMBER; i++) // row
  {
    for (int j = 0; j < KEYBOARD_COLUMN_NUMBER; j++) // column
    {
      // all FN keys are defined as 0x201 + index, the first is 0x201
      if (0 < (0x200 & g_key_lut[g_switch_config][0][i][j]))
      {
        g_fn_mask[i] |= 1 << j;
      }
    }
  }
}


/**
 * @brief  return the value of lower bits for keyboard layout configurations,
 *           if KEYBOARD_CONFIGURATION_COUNT is 11, the return is lower 4 bits content.
 *                          input is 10, output is 10
 *                          input is 11, output is 3,
 * @param  uint8_t switch_value
 * @retval switch_value, for valid bits value according to KEYBOARD_CONFIGURATION_COUNT
 * @intput KEYBOARD_CONFIGURATION_COUNT
 * @output g_fn_mask
 */
uint8_t get_valid_switch_config (uint8_t switch_value)
{
  if (KEYBOARD_CONFIGURATION_COUNT <= switch_value)
  {
    int switch_config_bits = 0;
    for (int i = 0 ; i < 8; i++)
    {
      if ((1 << i) >= KEYBOARD_CONFIGURATION_COUNT)
      {
        switch_config_bits = i;
        break;
      }
    }
    // if KEYBOARD_CONFIGURATION_COUNT == 11, from 0 to 10
    // switch_config_bits = 4,
    // (1<<switch_config_bits) -1 ) = 0b1111
    // input = 11, output = 3
    switch_value = (switch_value & ((1 << switch_config_bits) - 1));
    if (KEYBOARD_CONFIGURATION_COUNT <= switch_value)
    {
      switch_value = (switch_value & ((1 << (switch_config_bits - 1)) - 1));
    }
    return switch_value;
  }
  return switch_value;
}

/**
 * @brief  Read pressed keys in one row.
 *         TIM event        action         interval 25 us, 1ms / 40
 *             0         drive column 0
 *             1         read all row
 *             2         drive column 1
 *             3         read all row
 *
 *
 *
 *            32         transpose matrix, generate and send USB report
 *            33           NOP
 *            34           NOP
 *            35
 *            36
 *            37
 *            38
 *            39           NOP
 *
 * @param  int32_t row
 * @retval None
 * @intput g_keybuf[KEYBOARD_ROW_NUMBER]
 * @output g_keybuf[KEYBOARD_ROW_NUMBER]
 */
void TIM7_IRQHandler_callback (void)
{
  printf ("%ld\n", g_tim7_state);
  // scan keys row by row
  if ((KEYBOARD_COLUMN_NUMBER * 2) == g_tim7_state)   // 32 == g_tim7_state
  {
    // side effect
    transpose_matrix (g_key_buf_transposed,
                      g_key_buf,
                      1 + KEYBOARD_ROW_NUMBER, KEYBOARD_COLUMN_NUMBER); // 7, 16
    reverse_matrix_with_mask (g_key_buf, (KEYBOARD_ROW_NUMBER + 1)); // 16, function with side effect

    // g_switch_config = 0xff & (g_key_buf[KEYBOARD_ROW_NUMBER]); // side effect

    uint8_t temp_switch_config = get_valid_switch_config (g_key_buf[KEYBOARD_ROW_NUMBER]);
    if (g_switch_config_previous != temp_switch_config)
    {
      g_switch_config_previous  = g_switch_config;
      g_switch_config = temp_switch_config;
      fn_mask_init(); // side effect
    }

    keyboard_generate_report(); // side effect
    USBD_HID_SendReport (&hUsbDeviceFS, g_usb_report_buf, HID_BUFFER_SIZE); // 6.31 ms per packet, got from GDB
  }
  else if ((KEYBOARD_COLUMN_NUMBER * 2) < g_tim7_state) // 33 to 39
  {
    ;
  }
  else // [0, 31]
  {
    if (0 == (g_tim7_state & 0b1u)) // 0 == g_tim7_state % 2
    {
      // drive column
      keyboard_drive_column (g_tim7_state >> 1);
    }
    else // sampling
    {
      uint16_t key_read_column_value = 0;
      key_read_column_value = keyboard_read_column();
      uint32_t index = (g_tim7_state - 1) >> 1; // column number =
      g_key_buf_transposed[index] = key_read_column_value;
    }

    // if (KEYBOARD_COLUMN_NUMBER > g_scan_row)
    // {
    //   g_key_buf[g_scan_column] = keyboard_sampling_column (g_scan_column);
    //   keyboard_drive_column_low();

    //   g_tim7_state = 0;

    //   g_scan_column++;
    // }
    // else if (KEYBOARD_COLUMN_NUMBER <= g_scan_column) // uint32_t
    // {
    //   // USB error -71, maybe event -71 is always happen when unplug USB device
    //   g_tim7_state = 0;
    //   g_scan_column = 0;
    // }
    // else
    // {
    //   g_scan_column = 0;
    // }
  }

  g_tim7_state ++;
  // TODO: define 39
  if (g_tim7_state > 39)
  {
    g_tim7_state = 0;
  }
}

/**
 * @brief  Callback Function For TIM6
 * @param  None
 * @retval None
 * @output None
 */
void TIM6_DAC_IRQHandler_callback (void)
{
  g_led_scan_row++;
  // if (KEYBOARD_ROW_NUMBER <= g_led_scan_row)
  if (KEYBOARD_ROW_NUMBER <= g_led_scan_row)
  {
    g_led_scan_row = 0;
  }
  led_drive_row (g_led_scan_row_pattern[g_led_scan_row], g_led_scan_row);
}


/**
 * @brief  Drive one LED row
 * @param  uint16_t bit_binding, uint16_t row_number
 * @retval None
 * @output None
 */
void led_drive_row (uint16_t pattern, uint16_t row_number)
{
  union_half_word_byte_t pattern_half_word_byte;
  pattern_half_word_byte.half_word = pattern;
  if (HAL_OK != HAL_SPI_Transmit (&hspi2, pattern_half_word_byte.byte, 1, 1))
  {
    _Error_Handler (__FILE__, __LINE__);
  }
  GPIO_activate (SPI2_SS_GPIO_LED);
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  GPIO_deactivate (SPI2_SS_GPIO_LED);


  GPIO_deactivate (LED_ROW_DRIVE_00);
  GPIO_deactivate (LED_ROW_DRIVE_01);
  GPIO_deactivate (LED_ROW_DRIVE_02);
  GPIO_deactivate (LED_ROW_DRIVE_03);
  GPIO_deactivate (LED_ROW_DRIVE_04);
  GPIO_deactivate (LED_ROW_DRIVE_05);
  switch (g_led_scan_row)
  {
    default:
      break;
    case 00:
      GPIO_activate (LED_ROW_DRIVE_00);
      break;
    case 01:
      GPIO_activate (LED_ROW_DRIVE_01);
      break;
    case 02:
      GPIO_activate (LED_ROW_DRIVE_02);
      break;
    case 03:
      GPIO_activate (LED_ROW_DRIVE_03);
      break;
    case 04:
      GPIO_activate (LED_ROW_DRIVE_04);
      break;
    case 05:
      GPIO_activate (LED_ROW_DRIVE_05);
      break;
  }
}


/**
 * @brief  Drive column
 * @param  int32_t column
 * @retval None
 * @output None
 */
void keyboard_drive_column (uint32_t column)
{
  // column starts from 0
  // uint16_t column_number = 1 << column;
  assert_param (KEYBOARD_COLUMN_NUMBER > column);

  union_half_word_byte_t column_number;
  column_number.half_word = 1 << column;

  if (column >= KEYBOARD_COLUMN_NUMBER)
  {
    ;
  }
  else
  {
    if (HAL_OK != HAL_SPI_Transmit (&hspi1, & (column_number.byte[0]), 1, 1)) // delay no more than 1ms
    {
      _Error_Handler (__FILE__, __LINE__);
    }
    // TODO: related to endian, column_number is cast to uint8_t

    // for (int i = 0; i < 300; i++) {} // delay to wait SPI write finish
    GPIO_activate (SPI1_SS_GPIO_KEY);
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    GPIO_deactivate (SPI1_SS_GPIO_KEY);
  }
}


/**
 * @brief  set_lock_keys_indicator
 * @param  uint8_t USBState
 * @retval int error_number
 * @output None
 */
uint8_t set_lock_keys_indicator (uint8_t USBState)
{
  // D0:Num Lock   D1:Cap Lock   D2:Scroll Lock   D3:Compose   D4:Kana*/
  // sniff USB package with wireshark: filter: usb.transfer_type == 0x2
  // GPIO_CAPS_LOCK_LED3
  if (0 != (USBState & 0x02))
  {
    GPIO_activate (GPIO_CAPS_LOCK_LED3);
  }
  else
  {
    GPIO_deactivate (GPIO_CAPS_LOCK_LED3);
  }

  // GPIO_NUM_LOCK_LED2
  if (0 != (USBState & 0x01))
  {
    GPIO_activate (GPIO_NUM_LOCK_LED2);
  }
  else
  {
    GPIO_deactivate (GPIO_NUM_LOCK_LED2);
  }
  return 0;
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
// in TIM interrupt, we are going to have a 16 time slice
// in each of the slices, we initialize a SPI transfer
// and then we will wait about 62.5 micro seconds, we will read the pin status twice.
// at least the led driver need to be 16 kHz.
// seems too fast for key reading. It requires futher test.
// profilling required in this condition
// hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
// hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
// HAL_StatusTypeDef HAL_SPI_Transmit_IT(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size)
// |
// --------------------------- row
// |
// |
// |
// |
// |
// |
// |
// |
// |
// |
// column

/* USER CODE END 0 */

int main (void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  // also set NVIC priority group
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  __HAL_RCC_SYSCFG_CLK_ENABLE();
  HAL_EnableCompensationCell();

  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  // enable clock and set NVIC priority, set related GPIO of TIM, I2C, SPI
  HAL_TIM_Base_MspInit (&htim7);
  HAL_TIM_Base_MspInit (&htim6);
  HAL_TIM_Base_MspInit (&htim4);
  HAL_I2C_MspInit (&hi2c1);
  HAL_SPI_MspInit (&hspi1);
  HAL_SPI_MspInit (&hspi2);
  HAL_SPI_MspInit (&hspi3);
  HAL_UART_MspInit (&huart1);

  // enable TIM channel GPIO
  HAL_TIM_MspPostInit (&htim4);

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  GPIO_deactivate (GPIO_CAPS_LOCK_LED3);
  GPIO_deactivate (GPIO_NUM_LOCK_LED2);
  GPIO_deactivate (GPIO_SCROLL_LOCK_LED1);
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_SPI3_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();
  MX_TIM7_Init();
  MX_TIM6_Init();

  /* USER CODE BEGIN 2 */
  // TODO: fn_mask_init need to be update after every switch event.
  g_tim7_state = 0;
  HAL_Delay (1); // 1ms delay

  HAL_TIM_Base_Start_IT (&htim7);
  HAL_TIM_Base_Start_IT (&htim6);
  HAL_TIM_PWM_Start (&htim4, TIM_CHANNEL_3);

  // // HAL_TIM_OC_Start_IT(&htim4,3);

  // // TIM_OC3_SetConfig(htim->Instance, sConfig);
  // htim4.Instance->CCR3 = 16;

  fn_mask_init(); // do not delete, if 8 switch is 0, then g_switch_config
  // and g_switch_config_previous are the same, thus the program will not update g_fn_mask in TIM7 IRQ handler

  printf ("in main, before while\n");
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int cnt = -1;
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    // printf ("***\n");

    // if ( 0< (uwTick & (1<<16) )){}

    // g_switch_config should be bigger than 255, defined in MX_TIM4_Init()
    // htim4.Init.Period = 255;
    // __HAL_TIM_SET_COMPARE(__HANDLE__, __CHANNEL__, __COMPARE__)

    if (cnt >= 16)
    {
      cnt = 0;
      __HAL_TIM_SET_COMPARE (&htim4, TIM_CHANNEL_3, g_switch_config);
      GPIO_activate (GPIO_SCROLL_LOCK_LED1);
    }
    if (cnt == g_switch_config)
    {
      GPIO_deactivate (GPIO_SCROLL_LOCK_LED1);
    }
    cnt++;
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config (void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  /**Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG (PWR_REGULATOR_VOLTAGE_SCALE1);

  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig (&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig (&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  /**Configure the Systick interrupt time
  */
  HAL_SYSTICK_Config (HAL_RCC_GetHCLKFreq() / 1000);

  /**Configure the Systick
  */
  HAL_SYSTICK_CLKSourceConfig (SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority (SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init (void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init (&hi2c1) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* SPI1 init function */
static void MX_SPI1_Init (void)
{

  // APB2 84 MHz
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init (&hspi1) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* SPI2 init function */
static void MX_SPI2_Init (void)
{

  // APB1 42 MHz
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init (&hspi2) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* SPI3 init function */
static void MX_SPI3_Init (void)
{

  // APB1 42 MHz
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init (&hspi3) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init (void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 83;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 9999;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init (&htim3) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource (&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization (&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* TIM4 init function */
static void MX_TIM4_Init (void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 83;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 255;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init (&htim4) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource (&htim4, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_Init (&htim4) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization (&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 16;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel (&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit (&htim4);

}

/* TIM6 init function */
static void MX_TIM6_Init (void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  // TIM6 on APB1, APB1 speed 42 MHz, timer on APB1 speed * 2
  // Table 9. STM32F40x register boundary addresses (continued)
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 839;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 99;
  if (HAL_TIM_Base_Init (&htim6) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization (&htim6, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* TIM7 init function */
static void MX_TIM7_Init (void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  // TIM7 on APB1, APB1 speed 42 MHz, timer on APB1 speed * 2
  // Table 9. STM32F40x register boundary addresses (continued)
  // 164 us for every interrupt
  // 6097.56 interrupts per second
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 83;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 163;
  if (HAL_TIM_Base_Init (&htim7) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization (&htim7, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init (void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 921600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init (&huart1) != HAL_OK)
  {
    _Error_Handler (__FILE__, __LINE__);
  }

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init (void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin (GPIOA, EEPROM_nCS_Pin | EEPROM_nHOLD_Pin | EEPROM_nWP_Pin | SPI1_SS_GPIO_KEY_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin (GPIOC, LED_ROW_DRIVE_05_Pin | LED_ROW_DRIVE_04_Pin | LED_ROW_DRIVE_02_Pin | LED_ROW_DRIVE_01_Pin
                     | LED_ROW_DRIVE_00_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin (GPIOB, SPI2_SS_GPIO_LED_Pin | GPIO_CAPS_LOCK_LED3_Pin | GPIO_NUM_LOCK_LED2_Pin | GPIO_SCROLL_LOCK_LED1_Pin
                     | LED_ROW_DRIVE_03_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : KEY_ROW_02_Pin KEY_ROW_05_Pin KEY_ROW_04_Pin KEY_ROW_03_Pin */
  GPIO_InitStruct.Pin = KEY_ROW_02_Pin | KEY_ROW_05_Pin | KEY_ROW_04_Pin | KEY_ROW_03_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init (GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : KEY_ROW_01_Pin KEY_ROW_00_Pin KEY_ROW_06_Pin */
  GPIO_InitStruct.Pin = KEY_ROW_01_Pin | KEY_ROW_00_Pin | KEY_ROW_06_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init (GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : EEPROM_nCS_Pin EEPROM_nHOLD_Pin EEPROM_nWP_Pin SPI1_SS_GPIO_KEY_Pin */
  GPIO_InitStruct.Pin = EEPROM_nCS_Pin | EEPROM_nHOLD_Pin | EEPROM_nWP_Pin | SPI1_SS_GPIO_KEY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init (GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_ROW_DRIVE_05_Pin LED_ROW_DRIVE_04_Pin LED_ROW_DRIVE_02_Pin LED_ROW_DRIVE_01_Pin
                           LED_ROW_DRIVE_00_Pin */
  GPIO_InitStruct.Pin = LED_ROW_DRIVE_05_Pin | LED_ROW_DRIVE_04_Pin | LED_ROW_DRIVE_02_Pin | LED_ROW_DRIVE_01_Pin
                        | LED_ROW_DRIVE_00_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init (GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI2_SS_GPIO_LED_Pin GPIO_CAPS_LOCK_LED3_Pin GPIO_NUM_LOCK_LED2_Pin GPIO_SCROLL_LOCK_LED1_Pin */
  GPIO_InitStruct.Pin = SPI2_SS_GPIO_LED_Pin | GPIO_CAPS_LOCK_LED3_Pin | GPIO_NUM_LOCK_LED2_Pin | GPIO_SCROLL_LOCK_LED1_Pin
                        | LED_ROW_DRIVE_03_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init (GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : BOOT1_Pin */
  GPIO_InitStruct.Pin = BOOT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init (BOOT1_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
  // HAL_UART_Transmit_IT (&huart1, (uint8_t *) &ch, 1); // size == 1
  HAL_UART_Transmit (&huart1, (uint8_t *) &ch, 1, 1); // size == 1
  // HAL_UART_Transmit_IT(&huart1, ch, 1); // size == 1
  // USART_SendData (USART1, (uint8_t) ch);
  // ITM_SendChar ((uint8_t)ch);

  /* Loop until the end of transmission */
  // HAL_Delay (1);
  //  while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
  //  {}

  return ch;
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler (char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed (uint8_t *file, uint32_t line)
{
  while (1);
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */

/**
  * @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
